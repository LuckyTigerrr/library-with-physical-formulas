package ru.itis.formulas.model

import ru.itis.formulas.function.calculateFirstSpaceVelocity
import ru.itis.formulas.function.*

data class Planet(
    val radius: Double,
    val mass: Double,
    val atmosphereDensity: Double = 0.0,
    var gravitationalAcceleration: Double = 0.0,
    var firstSpaceVelocity: Double = 0.0,
    var secondSpaceVelocity: Double = 0.0
) {
    init {
        firstSpaceVelocity = calculateFirstSpaceVelocity()
        secondSpaceVelocity = calculateSecondSpaceVelocityByFirst()
        gravitationalAcceleration = calculateGravitationalAcceleration()
    }
}
